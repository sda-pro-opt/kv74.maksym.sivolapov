﻿#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

typedef struct
{
	int id;
	string name;
} column;

typedef struct
{
	char value;
	int type;
} symbol;

typedef struct {
	int id;
	int row;
	int position;
} lexeme;


vector<string> keyWords = {
	"PROGRAM",
	"BEGIN",
	"END",
	"LABEL",
	"GOTO",
	"LINK",
	"IN",
	"OUT"
};

vector<column> identifierTable, constantTable;
vector<lexeme> lexemeList;

ofstream resultFile;

void formIdTable() {
	int k = 401;
	column buffer;
	for (int i = 0; i < keyWords.size(); i++) {
		buffer.name = keyWords[i];
		buffer.id = k++;
		identifierTable.push_back(buffer);
	}
}

int searchConstant(string constant) {
	for (int i = 0; i < constantTable.size(); i++)
		if (constantTable[i].name == constant)
			return constantTable[i].id;
	return -1;
}

int addConstant(string constant) {
	column temp;
	temp.name = constant;
	if (constantTable.size() == 0)
		temp.id = 501;
	else
		temp.id = constantTable[constantTable.size() - 1].id + 1;
	constantTable.push_back(temp);
	return temp.id;
}

int searchIdenifier(string identifier) {
	for (int i = 0; i < identifierTable.size(); i++)
		if (identifierTable[i].name == identifier)
			return identifierTable[i].id;
	return -1;
}

int addIdentifier(string identifier) {
	column temp;
	temp.name = identifier;
	if ((identifierTable.size() == 0) || (identifierTable[identifierTable.size() - 1].id < 1000))
		temp.id = 1001;
	else
		temp.id = identifierTable[identifierTable.size() - 1].id + 1;
	identifierTable.push_back(temp);
	return temp.id;
}

int pos = 1;

symbol getNextSymbol(FILE* f) {
	symbol returnVal;
	pos++;
	fscanf(f, "%c", &returnVal.value);
	if ((returnVal.value == 32) || (returnVal.value == 13) || (returnVal.value == 10) ||
		(returnVal.value == 9) || (returnVal.value == 11) || (returnVal.value == 12))
		returnVal.type = 0;
	else if ((returnVal.value >= 48) && (returnVal.value <= 57))
		returnVal.type = 1;
	else if ((returnVal.value >= 65) && (returnVal.value <= 90))
		returnVal.type = 2;
	else if ((returnVal.value == 44) || (returnVal.value == 46) ||
		(returnVal.value == 58) || (returnVal.value == 59))
		returnVal.type = 3;
	else if (returnVal.value == 40)
		returnVal.type = 4;
	else
		returnVal.type = 5;
	return returnVal;
}


void error(const char* errorText,int row, int col) {
	resultFile << "ERROR("<<row<<", "<<col<< "): " << errorText << endl;
}

void lexicalAnalyzer(const char *inputFileName) {
	FILE* inputFile;
	inputFile = fopen(inputFileName, "r");
	if (inputFile == NULL)
	{
		cout << "File does not exist." << endl;
		return;
	}
	string buffer;
	int row = 1, firstPos;
	symbol s;
	int id = 0;
	bool addLexeme = true;
	s = getNextSymbol(inputFile);
	if (feof(inputFile))
		error("File is empty",0,0);
	pos = 1;
	while (!feof(inputFile)) {
		buffer = "";
		addLexeme = true;
		if (s.type == 0) {
			while (!feof(inputFile)) {
				if (s.value == '\n') {
					row++;
					pos = 0;
				}
				s = getNextSymbol(inputFile);
				
				if (s.type != 0)
					break;
			}
			addLexeme = false;
		}
		else if (s.type == 1)
		{
			firstPos = pos;
			while ((!feof(inputFile)) && (s.type == 1)) {
				buffer += s.value;
				s = getNextSymbol(inputFile);
			}
			id = searchConstant(buffer);
			if (id == -1)
				id = addConstant(buffer);
		}
		else if (s.type == 2)
		{
			firstPos = pos;
			while ((!feof(inputFile)) && ((s.type == 1) || (s.type == 2))) {
				buffer += s.value;
				s = getNextSymbol(inputFile);
			}
			id = searchIdenifier(buffer);
			if (id == -1)
				id = addIdentifier(buffer);
		}
		else if (s.type == 3)
		{
			firstPos = pos;
			id = s.value;
			s = getNextSymbol(inputFile);
		}
		else if (s.type == 4)
		{
			addLexeme = false;
			s = getNextSymbol(inputFile);
			if (feof(inputFile) || (s.value != '*'))
				error("Expected * after (",row,pos);
			s = getNextSymbol(inputFile);
			do {
				while ((!(feof(inputFile))) && (s.value != '*')) {
					if (s.value == '\n') {
						row++;
						pos = 0;
					}
					s = getNextSymbol(inputFile);
				}
				if (feof(inputFile))
				{
					error("Expected *) but end of the file found",row,pos);
					break;
				}
				s = getNextSymbol(inputFile);
			} while (s.value != ')');
			if (!feof(inputFile))
				s = getNextSymbol(inputFile);
		}
		else if (s.type == 5)
		{
			error("Illegal symbol",row,pos);
			s = getNextSymbol(inputFile);
			addLexeme = false;
		}
		if (addLexeme) {
			lexemeList.push_back(lexeme{ id,row,firstPos });
			//pos++;
		}
		
	}
	fclose(inputFile);
}



int main()
{
	formIdTable();
	string fileName;
	cout << "Enter test name" << endl;
	cin >> fileName;
	cout << endl;
	char* temp=new char[100];
	sprintf(temp, "tests/%s/input.sig",fileName.c_str());
	resultFile.open("tests/"+fileName+"/generated.txt");
	lexicalAnalyzer(temp);


	for (int i = 0; i < identifierTable.size(); i++)
		resultFile << identifierTable[i].id << " " << identifierTable[i].name << endl;
	resultFile << endl;
	for (int i = 0; i < constantTable.size(); i++)
		resultFile << constantTable[i].id << " " << constantTable[i].name << endl;
	resultFile << endl;
	for (int i = 0; i < lexemeList.size(); i++)
		resultFile << lexemeList[i].id << " " << lexemeList[i].row << " " << lexemeList[i].position << endl;
}
