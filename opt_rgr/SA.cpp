﻿#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

typedef struct
{
	int id;
	string name;
} column;

typedef struct
{
	char value;
	int type;
} symbol;

typedef struct {
	int id;
	int row;
	int position;
	bool parsed = false;
} lexeme;

typedef struct nodeSTR{
	string name;
	int lexemeId;
	vector<nodeSTR> children;
}node;


vector<string> keyWords = {
	"PROGRAM",
	"BEGIN",
	"END",
	"LABEL",
	"GOTO",
	"LINK",
	"IN",
	"OUT"
};

vector<column> identifierTable, constantTable;
vector<lexeme> lexemeList;
node syntaxTreeRoot;

ofstream resultFile;

void formIdTable() {
	int k = 401;
	column buffer;
	for (int i = 0; i < keyWords.size(); i++) {
		buffer.name = keyWords[i];
		buffer.id = k++;
		identifierTable.push_back(buffer);
	}
}

int searchConstant(string constant) {
	for (int i = 0; i < constantTable.size(); i++)
		if (constantTable[i].name == constant)
			return constantTable[i].id;
	return -1;
}

int addConstant(string constant) {
	column temp;
	temp.name = constant;
	if (constantTable.size() == 0)
		temp.id = 501;
	else
		temp.id = constantTable[constantTable.size() - 1].id + 1;
	constantTable.push_back(temp);
	return temp.id;
}

int searchIdenifier(string identifier) {
	for (int i = 0; i < identifierTable.size(); i++)
		if (identifierTable[i].name == identifier)
			return identifierTable[i].id;
	return -1;
}

int addIdentifier(string identifier) {
	column temp;
	temp.name = identifier;
	if ((identifierTable.size() == 0) || (identifierTable[identifierTable.size() - 1].id < 1000))
		temp.id = 1001;
	else
		temp.id = identifierTable[identifierTable.size() - 1].id + 1;
	identifierTable.push_back(temp);
	return temp.id;
}

string getNameByID(int id) {
	for (int i = 0; i < identifierTable.size(); i++) {
		if (identifierTable[i].id == id)
			return identifierTable[i].name;
	}
	for (int i = 0; i < constantTable.size(); i++) {
		if (constantTable[i].id == id)
			return constantTable[i].name;
	}
	string s;
	s = (char)id;
	return s;
}

int pos = 1;

symbol getNextSymbol(FILE* f) {
	symbol returnVal;
	pos++;
	fscanf(f, "%c", &returnVal.value);
	if ((returnVal.value == 32) || (returnVal.value == 13) || (returnVal.value == 10) ||
		(returnVal.value == 9) || (returnVal.value == 11) || (returnVal.value == 12))
		returnVal.type = 0;
	else if ((returnVal.value >= 48) && (returnVal.value <= 57))
		returnVal.type = 1;
	else if ((returnVal.value >= 65) && (returnVal.value <= 90))
		returnVal.type = 2;
	else if ((returnVal.value == 44) || (returnVal.value == 46) ||
		(returnVal.value == 58) || (returnVal.value == 59))
		returnVal.type = 3;
	else if (returnVal.value == 40)
		returnVal.type = 4;
	else
		returnVal.type = 5;
	return returnVal;
}


void error(const char* errorText, int row, int col) {
	resultFile << "ERROR(" << row << ", " << col << "): " << errorText << endl;
}

void lexicalAnalyzer(const char* inputFileName) {
	FILE* inputFile;
	inputFile = fopen(inputFileName, "r");
	if (inputFile == NULL)
	{
		cout << "File does not exist." << endl;
		return;
	}
	string buffer;
	int row = 1, firstPos;
	symbol s;
	int id = 0;
	bool addLexeme = true;
	s = getNextSymbol(inputFile);
	if (feof(inputFile))
		error("File is empty", 0, 0);
	pos = 1;
	while (!feof(inputFile)) {
		buffer = "";
		addLexeme = true;
		if (s.type == 0) {
			while (!feof(inputFile)) {
				if (s.value == '\n') {
					row++;
					pos = 0;
				}
				s = getNextSymbol(inputFile);

				if (s.type != 0)
					break;
			}
			addLexeme = false;
		}
		else if (s.type == 1)
		{
			firstPos = pos;
			while ((!feof(inputFile)) && (s.type == 1)) {
				buffer += s.value;
				s = getNextSymbol(inputFile);
			}
			id = searchConstant(buffer);
			if (id == -1)
				id = addConstant(buffer);
		}
		else if (s.type == 2)
		{
			firstPos = pos;
			while ((!feof(inputFile)) && ((s.type == 1) || (s.type == 2))) {
				buffer += s.value;
				s = getNextSymbol(inputFile);
			}
			id = searchIdenifier(buffer);
			if (id == -1)
				id = addIdentifier(buffer);
		}
		else if (s.type == 3)
		{
			firstPos = pos;
			id = s.value;
			s = getNextSymbol(inputFile);
		}
		else if (s.type == 4)
		{
			addLexeme = false;
			s = getNextSymbol(inputFile);
			if (feof(inputFile) || (s.value != '*'))
				error("Expected * after (", row, pos);
			s = getNextSymbol(inputFile);
			do {
				while ((!(feof(inputFile))) && (s.value != '*')) {
					if (s.value == '\n') {
						row++;
						pos = 0;
					}
					s = getNextSymbol(inputFile);
				}
				if (feof(inputFile))
				{
					error("Expected *) but end of the file found", row, pos);
					break;
				}
				s = getNextSymbol(inputFile);
			} while (s.value != ')');
			if (!feof(inputFile))
				s = getNextSymbol(inputFile);
		}
		else if (s.type == 5)
		{
			error("Illegal symbol", row, pos);
			s = getNextSymbol(inputFile);
			addLexeme = false;
		}
		if (addLexeme) {
			lexemeList.push_back(lexeme{ id,row,firstPos });
			//pos++;
		}

	}
	fclose(inputFile);
}

void identifier(node& subtree, int lexemeId) {
	node t1;
	t1.name = "<identifier>";
	node t2;
	t2.lexemeId = lexemeId;
	t1.children.push_back(t2);
	subtree.children.push_back(t1);
}


bool labels(node& subtree, int lexemeId) {
	if (lexemeList[lexemeId].id == 44) {
		node l1;
		l1.lexemeId = lexemeId;
		subtree.children.push_back(l1);
		lexemeList[lexemeId].parsed = true;
		lexemeId++;
		if (lexemeList[lexemeId].id < 501 || lexemeList[lexemeId].id >= 1000) {
			error("Expected unsigned integer", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
			return false;
		}
		node l2;
		l2.lexemeId = lexemeId;
		subtree.children.push_back(l2);
		lexemeList[lexemeId].parsed = true;
		return (labels(subtree,lexemeId+1));
	}
	return true;
}

bool labels_list(node& subtree, int lexemeId) {
	if (lexemeList[lexemeId].id != 404) {
		node ll1;
		ll1.name = "<empty>";
		subtree.children.push_back(ll1);
		return true;
	}
	node ll11;
	ll11.lexemeId = lexemeId;
	subtree.children.push_back(ll11);
	lexemeList[lexemeId].parsed = true;
	lexemeId++;
	if (lexemeList[lexemeId].id < 501 || lexemeList[lexemeId].id >= 1000) {
		error("Expected unsigned integer", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
		return false;
	}
	node ll2;
	ll2.lexemeId = lexemeId;
	lexemeList[lexemeId].parsed = true;
	subtree.children.push_back(ll2);
	lexemeId++;
	if (!labels(subtree, lexemeId))
		return false;
	while (lexemeId < lexemeList.size() - 1 && lexemeList[lexemeId].parsed)
		lexemeId++;
	if (lexemeList[lexemeId].id != 59) {
		error("Expected ;", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
		return false;
	}
	node ll3;
	ll3.lexemeId = lexemeId;
	lexemeList[lexemeId].parsed = true;
	subtree.children.push_back(ll3);
}

bool statement(node& subtree, int lexemeId) {
	node st1;
	st1.name = "<statement>";
	if (lexemeList[lexemeId].id > 500 && lexemeList[lexemeId].id <= 1000) {
		node st2;
		st2.lexemeId = lexemeId;
		lexemeList[lexemeId].parsed = true;
		st1.children.push_back(st2);
		lexemeId++;
		if (lexemeList[lexemeId].id != 58)
		{
			error("Expected :", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
			return false;
		}
		node st3;
		st3.lexemeId = lexemeId;
		lexemeList[lexemeId].parsed = true;
		st1.children.push_back(st3);
		lexemeId++;
		subtree.children.push_back(st1);
		return(statement(subtree, lexemeId));
	}
	if (lexemeList[lexemeId].id == 405|| lexemeList[lexemeId].id == 407|| lexemeList[lexemeId].id == 408) {
		node st2;
		st2.lexemeId = lexemeId;
		lexemeList[lexemeId].parsed = true;
		st1.children.push_back(st2);
		lexemeId++;
		if (lexemeList[lexemeId].id <= 500 || lexemeList[lexemeId].id > 1000) {
			error("Expected unsigned integer", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
			return false;
		}
		node st3;
		st3.lexemeId = lexemeId;
		lexemeList[lexemeId].parsed = true;
		st1.children.push_back(st3);
		lexemeId++;
		if (lexemeList[lexemeId].id != 59)
		{
			error("Expected ;", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
			return false;
		}
		node st4;
		st4.lexemeId = lexemeId;
		lexemeList[lexemeId].parsed = true;
		st1.children.push_back(st4);
		lexemeId++;
		subtree.children.push_back(st1);
		return(statement(subtree, lexemeId));
	}
	if (lexemeList[lexemeId].id == 406) {
		node st2;
		st2.lexemeId = lexemeId;
		lexemeList[lexemeId].parsed = true;
		st1.children.push_back(st2);
		lexemeId++;
		if (lexemeList[lexemeId].id <= 1000) {
			error("Expected variable identifier", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
			return false;
		}
		node st3;
		st3.name = "<variable-identifier>";
		identifier(st3, lexemeId);
		lexemeList[lexemeId].parsed = true;
		st1.children.push_back(st3);
		lexemeId++;
		if (lexemeList[lexemeId].id != 44)
		{
			error("Expected ,", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
			return false;
		}
		node st4;
		st4.lexemeId = lexemeId;
		lexemeList[lexemeId].parsed = true;
		st1.children.push_back(st4);
		lexemeId++;
		if (lexemeList[lexemeId].id <= 500 || lexemeList[lexemeId].id > 1000) {
			error("Expected unsigned integer", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
			return false;
		}
		node st5;
		st5.lexemeId = lexemeId;
		lexemeList[lexemeId].parsed = true;
		st1.children.push_back(st5);
		lexemeId++;
		if (lexemeList[lexemeId].id != 59)
		{
			error("Expected ;", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
			return false;
		}
		node st6;
		st6.lexemeId = lexemeId;
		lexemeList[lexemeId].parsed = true;
		st1.children.push_back(st6);
		lexemeId++;
		subtree.children.push_back(st1);
		return(statement(subtree, lexemeId));
	}
	return true;
}


bool block(node& subtree, int lexemeId) {
	node bl1;
	bl1.name = "<declarations>";
	node bl2;
	bl2.name = "<label-declarations>";
	if (!labels_list(bl2, lexemeId))
		return false;
	bl1.children.push_back(bl2);
	subtree.children.push_back(bl1);
	while (lexemeId < lexemeList.size() - 1 && lexemeList[lexemeId].parsed)
		lexemeId++;
	if (lexemeList[lexemeId].id != 402) {
		error("Expected BEGIN", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
		return false;
	}
	node bl3;
	bl3.lexemeId=lexemeId;
	lexemeList[lexemeId].parsed = true;
	subtree.children.push_back(bl3);
	lexemeId++;


	node bl4;
	bl4.name = "<statements-list>";
	if (!statement(bl4, lexemeId))
		return false;
	if (bl4.children.size() == 0){
		node blt;
		blt.name = "<empty>";
		bl4.children.push_back(blt);
	}
	subtree.children.push_back(bl4);

	while (lexemeId < lexemeList.size() - 1 && lexemeList[lexemeId].parsed)
		lexemeId++;
	if (lexemeList[lexemeId].id != 403) {
		error("Expected END", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
		return false;
	}
	node bl5;
	bl5.lexemeId = lexemeId;
	lexemeList[lexemeId].parsed = true;
	subtree.children.push_back(bl5);
	return true;
}

bool program(node& subtree,int lexemeId) {
	if (lexemeList[lexemeId].id != 401){
		error("Expected PROGRAM", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
		return false;
	}
	node pr1;
	pr1.lexemeId = lexemeId;
	lexemeList[lexemeId].parsed = true;
	subtree.children.push_back(pr1);
	lexemeId++;
	if (lexemeList[lexemeId].id <= 1000) {
		error("Expected program identifier", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
		return false;
	}
	node pr2;
	pr2.name = "<procedure-identifier>";
	identifier(pr2, lexemeId);
	lexemeList[lexemeId].parsed = true;
	subtree.children.push_back(pr2);
	lexemeId++;
	if (lexemeList[lexemeId].id != 59) {
		error("Expected ;", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
		return false;
	}
	node pr3;
	pr3.lexemeId = lexemeId;
	lexemeList[lexemeId].parsed = true;
	subtree.children.push_back(pr3);

	lexemeId++;
	node pr4;
	pr4.name = "<block>";
	if (!block(pr4, lexemeId))
		return false;
	subtree.children.push_back(pr4);

	while (lexemeId < lexemeList.size()-1&&lexemeList[lexemeId].parsed)
		lexemeId++;
	if (lexemeList[lexemeId].id == 46) {
		node pr5;
		pr5.lexemeId = lexemeId;
		subtree.children.push_back(pr5);
		lexemeList[lexemeId].parsed = true;
		return true;
	}
	error("Expected .", lexemeList[lexemeId].row, lexemeList[lexemeId].position);
	return false;
}




void syntaxAnalyzer() {
	syntaxTreeRoot.name = "<signal-program>";
	syntaxTreeRoot.lexemeId = -1;
	node child;
	child.name = "<program>";
	child.lexemeId = -1;
	syntaxTreeRoot.children.push_back(child);
	program(syntaxTreeRoot.children[0], 0);
}



void printTree(ostream &out,node& subtree,int depth) {
	string buffer;
	for (int i = 0; i < depth; i++)
		buffer += "--";
	if (subtree.name != "")
		buffer += subtree.name;
	else
		buffer += to_string(lexemeList[subtree.lexemeId].id) + " " + getNameByID(lexemeList[subtree.lexemeId].id);
	out << buffer << endl;
	for (int i = 0; i < subtree.children.size(); i++)
		printTree(out,subtree.children[i], depth + 1);
}

int main()
{
	formIdTable();
	string fileName;
	cout << "Enter test name" << endl;
	cin >> fileName;
	cout << endl;
	char* temp = new char[100];
	sprintf(temp, "tests/%s/input.sig", fileName.c_str());
	resultFile.open("tests/" + fileName + "/generated.txt");
	lexicalAnalyzer(temp);
	syntaxAnalyzer();
	printTree(resultFile,syntaxTreeRoot, 0);
	return 0;
}
